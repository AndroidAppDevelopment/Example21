package rs.aleph.android.example21.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import rs.aleph.android.example21.R;
import rs.aleph.android.example21.adapters.ProductsRecyclerViewAdapter;
import rs.aleph.android.example21.model.Product;
import rs.aleph.android.example21.provider.ProductProvider;

public class ListFragment extends Fragment implements ProductsRecyclerViewAdapter.OnProductClickedListener{

    // Container Activity must implement this interface
    public interface OnProductSelectedListener {
        void onProductSelected(int id);
    }

    OnProductSelectedListener listener;
    RecyclerView rvProducts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rvProducts = (RecyclerView) getView().findViewById(R.id.products);
        new GetProductsAsyncTask().execute();
    }

    private void setupProductList(List<Product> fruits) {
        rvProducts.setLayoutManager(new LinearLayoutManager(getActivity()));
        ProductsRecyclerViewAdapter adapter = new ProductsRecyclerViewAdapter(fruits, this);
        rvProducts.setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.list_fragment, container, false);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
           listener = (OnProductSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnProductSelectedListener");
        }
    }

    @Override
    public void onProductClicked(int id) {
        if (listener != null){
            listener.onProductSelected(id);
        }
    }

    public class GetProductsAsyncTask extends AsyncTask<Void, Void, List<Product>> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ListFragment.this.getActivity());
            progressDialog.setMessage("Loading fruits...");
            progressDialog.show();
        }

        @Override
        protected List<Product> doInBackground(Void... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e){
                e.printStackTrace();
                return null;
            }
            return ProductProvider.getProducts();
        }

        @Override
        protected void onPostExecute(List<Product> products) {
            super.onPostExecute(products);
            if (progressDialog != null){
                progressDialog.dismiss();
            }
            setupProductList(products);
        }
    }
}

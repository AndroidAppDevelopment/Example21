package rs.aleph.android.example21.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import rs.aleph.android.example21.R;
import rs.aleph.android.example21.model.Product;

public class ProductsRecyclerViewAdapter extends RecyclerView.Adapter<ProductsRecyclerViewAdapter.ViewHolder> {

    private List<Product> products;
    private OnProductClickedListener listener;

    public ProductsRecyclerViewAdapter(List<Product> productList, OnProductClickedListener listener){
        products = productList;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView tv = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(tv);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int productId = products.get(position).getId();
        holder.tvProductName.setText(products.get(position).getName());
        holder.tvProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onProductClicked(productId);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvProductName;
        ViewHolder(View itemView) {
            super(itemView);
            tvProductName = (TextView) itemView;
        }
    }

    public interface OnProductClickedListener{
        void onProductClicked(int id);
    }
}
